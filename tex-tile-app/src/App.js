import React, {Component} from 'react';
import {BrowserRouter,Switch,Route} from "react-router-dom";

import Home from "./container/Home/Home";
import Footer from "./components/Footer/Footer";
import MenuBar from "./components/MenuBar/MenuBar";
import ProductsDisplay from "./container/ProductsDisplay/ProductsDisplay";
import ProductDesc from "./container/ProductDesc/ProductDesc";
import SplashScreen from "./components/Splash_Screen/Splash_Screen";
import './App.css'


class App extends Component {

    constructor(props){
        super(props);

        this.state = {
            prevScrollpos: window.pageYOffset,
        }
    }

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll = () => {
        const { prevScrollpos } = this.state;

        const currentScrollPos = window.pageYOffset;
        console.log(currentScrollPos)
        const visible = prevScrollpos > currentScrollPos;
        console.log(visible);
        this.setState({
            prevScrollpos: currentScrollPos,
            visible
        });
    };

    render() {

        console.log(this.state.prevScrollpos);

        return (
            <div>
                {/*<SplashScreen/>*/}
               <BrowserRouter>
                   <MenuBar/>
                   <Switch>
                       <Route path="/"  exact component={Home}/>

                       {/*<Route path="/"  component={SplashScreen} />*/}

                       <Route path="/products_list/product-view" component={ProductDesc}/>
                       <Route path="/products_list" component={ProductsDisplay}/>

                   </Switch>
                   <Footer/>
               </BrowserRouter>
            </div>
        );
    }
}

export default App;