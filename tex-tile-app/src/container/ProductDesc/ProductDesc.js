import React, {Component} from 'react';
import "./ProductDesc.css"

import FullProduct from "../../assets/image/Products/ProductDesc/Screenshot from 2019-09-08 07-49-59.png"
import Product1 from "../../assets/image/Products/ProductDesc/Screenshot from 2019-09-08 07-50-18.png"
import Product2 from "../../assets/image/Products/ProductDesc/Screenshot from 2019-09-08 07-50-28.png"
import Product3 from "../../assets/image/Products/ProductDesc/Screenshot from 2019-09-08 07-50-38.png"
import Product4 from "../../assets/image/Products/ProductDesc/Screenshot from 2019-09-08 07-50-48.png"


class ProductDesc extends Component {
    constructor(props){
        super(props);
        window.scroll({top:0});
    }
    render() {
        return (
            <div className="product_desc_container">
                <div className="product_desc_view_product">
                   <div className="product_desc_image">
                       <img src={FullProduct} alt="none"/>
                   </div>
                    <div className="product_desc_content">
                        <div className="product_desc_content_mrp">
                            <span>&#8377; 2,590.00</span>
                            <h6>MRP incl of all taxes</h6>
                            <p>BROWN - 5680/247</p>
                        </div>
                        <div className="product_desc_content_size_box">
                            <div className="size_info">
                                <p>S</p>
                                <div>
                                    <i className="far fa-envelope"/> Coming Soon
                                </div>
                            </div>
                            <div className="size_info">
                                <p>M</p>
                            </div>
                            <div className="active">
                                <p>L</p>
                            </div>
                            <div className="size_info">
                                <p>XL</p>
                            </div>
                            <div className="product_desc_content_size_box_footer">
                                <i className="far fa-envelope"/> We'll let you know when it's in stock
                            </div>
                        </div>
                        <div className="size_button">
                            <p>WHAT'S MY SIZE?</p>
                        </div>
                        <div className="add">
                            <button>CART</button>
                        </div>
                    </div>
                </div>
                <div className="product_desc_image_bottom">
                    <img src={Product1} alt="none"/>
                    <img src={Product2} alt="none"/>
                    <img src={Product3} alt="none"/>
                    <img src={Product4} alt="none"/>
                </div>


                {/*{ similar products views}*/}

                <div className="similar_product">

                </div>
            </div>
        );
    }
}

export default ProductDesc;