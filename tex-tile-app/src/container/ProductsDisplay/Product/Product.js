import React, {Component} from 'react';
import "./Product.css"
import  Product1 from "../../../assets/image/Products/Screenshot from 2019-09-08 08-04-23.png";
import Product2 from "../../../assets/image/Products/Screenshot from 2019-09-08 08-04-38.png";
import Product3 from "../../../assets/image/Products/Screenshot from 2019-09-08 08-05-04.png";
class Product extends Component {
    render() {
        return (
            <div className="product">
                <div className="product_image">
                    <a href="/products_list/product-view" target="_blank" >
                        <img src={ Product1}alt="PRODUCT"/>
                    </a>
                </div>
                <div className="product_hover">
                    <div className="product_footer">
                        <h6>Product Name</h6>
                        <p>&#8377;<strike>222</strike><span> &#8377;111</span></p>
                    </div>

                </div>
            </div>
        );
    }
}

export default Product;