import React, {Component} from 'react';
import "./Footer.css"
import icon from "../../assets/image/footer-icon.png"
import { Link} from "react-router-dom";

class Footer extends Component {

    state = {
        news_letter : '',
    };

    handlerChange = (e) => {
        this.setState({[e.target.name] : e.target.value})
    };

    render() {
        return (
            <div className="footer_container">
               <div>
                   <img src={icon} alt="none"/>
               </div>
                <div className="footer_content">
                    <form>
                        <div className="footer_content_row_1">
                            <div className="footer_content_row_1_1">
                                <div className="footer_content_row_1_1_heading">
                                    NEWSLETTER
                                </div>
                                <div className="footer_content_row_1_1_text">
                                    <input type="email"
                                           name="news_letter"
                                           value={this.state.news_letter}
                                           onChange={this.handlerChange}
                                           placeholder="Enter your mail id"
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div className="footer_content_row_2">
                        <div className="footer_content_row_2_1">
                            <div className="footer_content_row_2_1_heading">
                                <h5>Shop</h5>
                            </div>
                            <div className="footer_content_row_2_1_link">
                                <Link to="#">Shop</Link>
                                <Link to="#">Shirt</Link>
                                <Link to="#">Loungewear</Link>
                                <Link to="#">Collections</Link>
                            </div>
                        </div>
                        <div className="footer_content_row_2_1">
                            <div className="footer_content_row_2_1_heading">
                                <h5>Info</h5>
                            </div>
                            <div className="footer_content_row_2_1_link">
                                <Link to="#">About</Link>
                                <Link to="#">Our Shops</Link>
                                <Link to="#">Journal</Link>
                                <Link to="#">Contact Us</Link>
                                <Link to="#">Our Careers</Link>
                            </div>
                        </div>
                        <div className="footer_content_row_2_1">
                            <div className="footer_content_row_2_1_heading">
                                <h5>Customer</h5>
                            </div>
                            <div className="footer_content_row_2_1_link">
                                <Link to="#">My Account</Link>
                                <Link to="#">Delivery & Returns</Link>
                                <Link to="#">Terms & Conditions</Link>
                                <Link to="#">Privacy Policy</Link>
                            </div>
                        </div>
                        <div className="footer_content_row_2_1">
                            <div className="footer_content_row_2_1_heading">
                                <h5>Follow Us</h5>
                            </div>
                            <div className="footer_content_row_2_1_link">
                                <Link to="#">Facebook</Link>
                                <Link to="#">Instagram</Link>
                                <Link to="#">Twitter</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;