import React, {Component} from 'react';
import './FirstBanner.css'
import FirstImage from "../../assets/image/Screenshot from 2019-09-01 08-46-26.png"
import SecondImage from "../../assets/image/Screenshot from 2019-09-01 08-46-51.png"
import ThirdImage from "../../assets/image/Screenshot from 2019-09-01 08-47-04.png"
import { Link } from "react-router-dom";

class FirstBanner extends Component {
    render() {
        return (
            <div className="first_banner_container">
                <div className="first_banner_img">
                    <Link to="">
                        <img src={FirstImage} alt="none"/>
                    </Link>
                </div>
                <div className="first_banner_img">
                    <Link to="#">
                        <img src={SecondImage} alt="none"/>
                    </Link>
                </div>
               <div className="first_banner_img">
                   <Link to="#">
                       <img src={ThirdImage} alt="none"/>
                   </Link>
               </div>
            </div>
        );
    }
}

export default FirstBanner;