import React, {Component} from 'react';
import './FourthBanner.css'
import fourthBanner from "../../assets/image/fourthbanner.png"
class FourthBanner extends Component {
    render() {
        return (
            <div className="fourth_banner_container">
                <div className="fourth_banner_container_image">
                    <img src={fourthBanner} alt="none"/>
                </div>
                <div className="fourth_banner_content">
                    <div className="fourth_banner_right_header">
                        The
                        Holiday
                        Checks
                    </div>
                    <div className="fourth_banner_right_content">
                        {/*<h3> Truly One of a Kind.</h3>*/}
                        <p> Whether you’re staying near or venturing far,
                            make this winter your best dressed yet.</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default FourthBanner;