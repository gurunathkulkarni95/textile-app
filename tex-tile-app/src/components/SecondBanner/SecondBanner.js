import React, {Component} from 'react';
import './SecondBanner.css'
import sec_one_image from "../../assets/image/sec_one_image.jpg"
import sec_two_image from "../../assets/image/sec_two_image.jpg"
import {Link} from "react-router-dom"


class SecondBanner extends Component {
    render() {
        return (
            <div className="second_banner_container">
                <div className="second_banner_content">
                    <h3>New In</h3>
                    <Link to="/products_list">
                        <img src={sec_one_image} alt="none"/>
                    </Link>
                    <h4>MODVOLK</h4>
                    <p>Celebrating the new generation gentlemen</p>
                </div>
                <div className="second_banner_right_content">
                    <Link to="/products_list">
                    <img src={sec_two_image} alt="none"/>
                    </Link>
                </div>
            </div>
        );
    }
}

export default SecondBanner;