import React, {Component} from 'react';
import "./ThirdBanner.css"
import thirdBanner from "../../assets/image/thirdbanner.png"

class ThirdBanner extends Component {
    render() {
        return (
            <div className="third_banner_container">
                <div className="third_banner_left_part">
                   <div className="third_banner_left_header">
                       WASHED INDIGO
                   </div>
                   <div className="third_banner_left_content">
                       <h3> Truly One of a Kind.</h3>
                       <p> You simply won’t find washed indigo
                           shirts like this anywhere else.</p>
                   </div>
                    <div className="button_class">
                       <button className="button_class_view"> view </button>
                       <button className="button_class_collection"> collection</button>
                    </div>
                </div>
                <div className="third_banner_right_part">
                    <img src={thirdBanner} alt="none"/>
                </div>
            </div>
        );
    }
}

export default ThirdBanner;